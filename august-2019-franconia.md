# August Franconia hike
Looks like there's gonna be some rain, so we're gonna skip the classic Franconian Notch loop (if it's dry on Sunday we might be able to do it), and go for some nearby trails. 

These trails have amazing views on clear days (some have great views even on cloudy days)

We'll be camping at [Lafayette Campground](https://www.google.com/search?newwindow=1&client=firefox-b-1-d&sa=X&q=Lafayette%20Place%20Campground&stick=H4sIAAAAAAAAAONgecSYyS3w8sc9YamESWtOXmOM4uIKzsgvd80rySypFPLhYoOyFLj4pbj10_UNS1JSDCzSszUYpHi5kAWkFJS4eMU2-0uJPrjkelVLiHO_90-L6potPoJtHrrnp3VLcvIsYpXySUxLrEwtKUlVCMhJTE5VcE7MLUgvyi_NSwEAZ8FTRosAAAA&ved=2ahUKEwjr87OY9vHjAhUQX80KHfkwCjgQ6RMwGnoECAoQBA&biw=1283&bih=831), and essentially drive up to where we're tenting. 

Parking on saturday, before camping checkin, will be at [Cannon Mtn Gondola](https://www.google.com/search?client=firefox-b-1-d&q=cannon+mountain+gondola)

Note the hike is set up to be very flexible, so we can decide while on the trail what we want to do, as described below.

## Weather and environment
As mentioned, we'll be expecting some rain on Saturday. For temperatures, bring cloths for down to 35-40 °F at night, and 40 °F above treeline when we hit the ridges. Be able to dress down to ~60 °F

Starting out we'll be below tree line, with bugs and such. Note its tic season, so even if you're going in shorts, have long socks and bug spray. I have deet, basically the rocket fuel of bug sprays.

## Timing
We want to be at campsite by 10:00 am, thus we'll leave Beverly at 7:30 am Saturday

Check in for our campsite is at 1:00 pm, with checkout being at 11:00 am Sunday

## Trails
We'll get a feel for what trails to hit after the first one, and see where everyone's at. Below are the maps for each possible trail, depending on what we decide to do.

### Cannon Mt Loop Trail - Main
[Here's the main trail](https://www.alltrails.com/explore/map/map--28139) we'll start off from. From here, we can also split off to the purple trail down towards the waypoint set, where we can get crazy good views of the entire valley there. On our way back, we can go down 'High Cannon Trail' (blue trail) for a change of scenery

Another option on this trail is the green route, so we can hit Kinsman as well. 

Note the distance/elevation numbers are a bit off

### Mt Flume
Another option, perhaps Sunday, is to hit [Mt Flume](https://www.alltrails.com/explore/map/map--28145) after we check out from our camp site. It's an out and back trail (so we'd see the same trail after we summit) but great views from here as well.

## Food
We'll essentially have the car next to the tent site, so any food we want for the night we can store in a cooler in the car, and bring snacks with us on the trail. Along the trail there are water sources, from freshwater to river/spring water. I have a water filter we can use. 

##  Supplies
We have an 8-person tent, with four sleeping bags and a ;arge mattress pad, so we're all set for sleeping. Plus, with the car nearby, if we need to we can always drive to the nearby town and pick something up if needed. Beyond that, typical supplies for hiking, such as:

 - Extra socks - wet feet will be more prone to blisters. If you have the choice, avoid cotton, go for wool/synthetic. If not, no big deal.
 - Rain/wind gear - A wind breaker/rain jacket will be good above tree line, and if we end up getting some showers
 - Lite, long pants - Even though its  summer, long pants will be hugely helpful to combat bug bites, and especially protect against tics. I'd bring shorts, too. Or be a cool dad-bro and have long pants that turn into shorts like me. #stylin
 - Long sleeve shirt + short sleeve shirt - Again, even though its summer, cloths work better than bare skin and bug spray
 - Hat

Note this list isn't exhaustive, but rather just some notable items.

For backpacks, normal school backpacks will work, since we don't need to carry the tent or large amounts of food. 

I typically bring two Nalgene bottles (total ~64 ounces), or a camel bak (water pouch). Note we'll have opportunities to refill on most trails.

